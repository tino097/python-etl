
import requests
import ndjson
from sqlalchemy.orm import sessionmaker

import pandas as pd

from models.models import Patient


def load_data(engine):
    patients = []
    response = requests.get('https://raw.githubusercontent.com/smart-on-fhir/flat-fhir-files/master/r3/Patient.ndjson')
    patient_data = response.json(cls=ndjson.Decoder)
    df = pd.json_normalize(patient_data)
    for index, row in df.iterrows():
        source_id = row.id
        birth_date = row.birthDate
        gender = row.gender
        country = row.address[0].get('country')
        for ext in row.extension:
            if ext['url'] == 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-race':
               race_code = ext['valueCodeableConcept']['coding'][0]['code']
               race_code_system = ext['valueCodeableConcept']['coding'][0]['system']
            if ext['url'] == 'http://hl7.org/fhir/us/core/StructureDefinition/us-core-ethnicity':
                ethnicity_code = ext['valueCodeableConcept']['coding'][0]['code']
                ethnicity_code_system = ext['valueCodeableConcept']['coding'][0]['system']
        
        patient = Patient(source_id, birth_date, gender, race_code, race_code_system, ethnicity_code, ethnicity_code_system,country)
        patients.append(patient)
        
    return patients



    
   
    