from config import create_db
from models import create_models
from api.api import load_data



if __name__ == "__main__":
    engine = create_db()
    create_models(engine)

    patients = load_data(engine)
    save_data(patients)