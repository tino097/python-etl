import sqlalchemy as db
from sqlalchemy.ext.declarative import declarative_base
import logging

from sqlalchemy_utils import database_exists, create_database
from psycopg2 import OperationalError

logging.basicConfig(level=logging.INFO)

Base = declarative_base()


def create_db():
    
    logging.info("Prepare database engine")
    try:
        engine = db.create_engine('postgresql://postgres:postgres@localhost:5432/FHIR', echo=True)
    except (Exception, OperationalError) as err:
        engine = None
        logging.info(err)

    if not database_exists(engine.url):
        logging.info("Database not found. Attempting to crate it")
        create_database(engine.url)
        
    connection = engine.connect()
    logging.info("Succesfully created database")
    connection.execute("commit")

    logging.info("Creating tables ...")

    return engine
