
from models.models import Patient, Observation, Procedure, Encounter
from config import Base


def create_models(engine):
    Base.metadata.create_all(engine)