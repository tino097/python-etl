
from config import Base
from sqlalchemy import (Column, Integer, Numeric, String, Date, DateTime, ForeignKey)
from sqlalchemy.orm import relationship

class Patient(Base):
    __tablename__ = 'patient'

    id = Column(Integer, primary_key=True)
    source_id = Column(String, nullable=False)
    birth_date = Column(Date)
    gender = Column(String)
    race_code = Column(String)
    race_code_system = Column(String)
    ethnicity_code = Column(String)
    ethnicity_code_system = Column(String)
    country = Column(String)


    def __init__(self, source_id, birth_date, gender, race_code, race_code_system, ethnicity_code,
                 ethnicity_code_system, country):
        self.source_id = source_id
        self.birth_date = birth_date
        self.gender = gender
        self.race_code = race_code
        self.race_code_system = race_code_system
        self.ethnicity_code = ethnicity_code
        self.ethnicity_code_system = ethnicity_code_system
        self.country = country

    def __repr__(self):
        return "<Patient (source_id = %s)>" % self.source_id 

class Encounter(Base):
    __tablename__ = 'encounter'

    id = Column(Integer, primary_key=True)
    source_id = Column(String, nullable=False)
    patient_id = Column(Integer, ForeignKey('patient.id'))
    #patient = relationship("Patient", back_populates="encounter")
    start_date = Column(DateTime, nullable=False)
    end_date = Column(DateTime, nullable=False)
    type_code = Column(String)
    type_code_system = Column(String)
    

    def __init__(self, source_id, patient_id, start_date, end_date, type_code, type_code_system):
        self.source_id = source_id
        self.patient_id = patient_id
        self.start_date = start_date
        self.end_date = end_date
        self.type_code = type_code
        self.type_code_system = type_code_system


    def __repr__(self):
        return "<Encounter (source_id = %s>" % self.source_id


class Procedure(Base):
    __tablename__ = 'procedure'

    id = Column(Integer, primary_key=True)
    source_id = Column(String, nullable=False)
    patient_id = Column(Integer, ForeignKey('patient.id'))
    #patient = relationship("Patient", back_populates="procedure")
    encounter_id = Column(Integer, ForeignKey('encounter.id'))
    #encounter = relationship("Encounter", back_populates="procedure")
    procedure_date = Column(Date, nullable=False)
    type_code = Column(String, nullable=False)
    type_code_system = Column(String, nullable=False)

    def __init__(self, source_id, patient_id, encounter_id, procedure_date, type_code, type_code_system):

        self.source_id = source_id
        self.patient_id = patient_id
        self.encounter_id = encounter_id
        self.procedure_date = procedure_date
        self.type_code = type_code
        self.type_code_system = type_code_system

    def __repr__(self):
        return "<Procedure (source_id = %s>" % self.source_id

class Observation(Base):
    __tablename__ = 'observation'

    id = Column(Integer, primary_key=True)
    source_id = Column(String, nullable=False)
    patient_id = Column(Integer, ForeignKey('patient.id'))
    #patient = relationship("Patient", back_populates="procedure")
    encounter_id = Column(Integer, ForeignKey('encounter.id'))
    #encounter = relationship("Encounter", back_populates="procedure")
    observation_date = Column(Date, nullable=False)
    type_code = Column(String, nullable=False)
    type_code_system = Column(String, nullable=False)
    value = Column(Numeric, nullable=False)
    unit_code = Column(String)
    unit_code_system = Column(String)

    def __init__(self, source_id, patient_id, encounter_id, observation_date, type_code, type_code_system, value,
                unit_code, unit_code_system):

        self.source_id = source_id
        self.patient_id = patient_id
        self.encounter_id = encounter_id
        self.procedure_date = procedure_date
        self.type_code = type_code
        self.type_code_system = type_code_system

    def __repr__(self):
        return "<Observation (source_id = %s>" % self.source_id
 
 
